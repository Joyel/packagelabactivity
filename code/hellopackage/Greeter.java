package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

/**
 * Greeter is a class used to test packages, using a quick test of doubling an int.
 * @author Joyel Selvakumar
 * @version 8/29/2022
 */

public class Greeter {

    /**
     * Main method for asking int input and sending to the doubling method. Also displays the doubled int.
     * 
     * @param args Optional command-line arguments to enter at program runtime.
     */
    public static void main(String[] args){
        Scanner read = new Scanner(System.in);

        Random randgen = new Random();

        System.out.println("Enter an integer");
        int testNum = read.nextInt();

        int testNumDoubled = Utilities.doubleMe(testNum);
        System.out.println(testNumDoubled);
    }
}
