package secondpackage;

/**
 * Utilities contains the integer doubling method/utility.
 * @author Joyel Selvakumar
 * @version 8/29/2022
 */

public class Utilities {

    /**
     * Doubles the input value and returns that.
     * 
     * @param x The int that will be doubled
     * @return Returns x * 2.
     */

    public static int doubleMe(int x){
        return (x *2);
    }
}
